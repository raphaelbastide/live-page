<?php
  $img = file_get_contents("php://input"); // $_POST didn't work
  $tagged = false;
  if (strpos( $img, "tagged!!" ) !== false) { // if image is tagged
    $img = str_replace('tagged!!', '', $img);
    $tagged = true;
  }
  $img = str_replace('data:image/jpeg;base64,', '', $img);
  $img = str_replace(' ', '+', $img);
  $data = base64_decode($img);
  
  if (!file_exists("/")) {
    mkdir("/invitation-images/", 0777, true);
  }
  if ($tagged) {
    $file = "invitation-images/".time().' drawing .jpeg';  
  }else{
    $file = "invitation-images/".time().'.jpeg';  
  }
  $success = file_put_contents($file, $data);
  print $success ? $file.' saved.' : 'Unable to save the file.';