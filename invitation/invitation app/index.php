<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>A party to Print | Invitation</title>
    <link rel="stylesheet" href="font/stylesheet.css">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <style id="maincol">:root{--col:cyan;}</style>
  </head>
  <body>
    <p>A Party to print <br> at HFG 29/06/2023 10PM <br> Suggested donation: 2€</p>
    <!-- <label for="colors">Choose a color:</label>
    <select name="colors" id="colors" onChange="changeColor(this.value);">
      <option value="blueviolet" default>Blueviolet</option>
      <option value="cyan">Cyan</option>
    </select>  -->

    <p class="enough">Enough pictures. <br> See you on the dancefloor.</p>
    <!-- <p class="instructions-ios">Draw something <br> and save it to the book</p>
    <div class="contentarea">
      <div id="output">
        <canvas id="sketchpad" width='300' height='320'></canvas>
        <div class="color-overlay"></div>
        <canvas id="canvas"></canvas>
        <img id="photo">
        <video id="video">Video stream not available.</video>
        <div class="mask-container">
          <div class="mask"></div>
        </div>
      </div>
      <img class="logo" src="logo.png" alt="">
    </div> 
    <div class="nav">
      <button id="startbutton">photo</button>
      <button id="clearBtn" onClick="location.reload(true);">Redo</button>
      <button id="downloadLnk" disabled onClick="download()">Save</button>
    </div>
    -->
    <p class="disclamer">We use personal data such as pictures, videos and more for the later publication of  ”Blur Factory”. <br>
By attending this event, you agree to be photographed, recorded and possibly published.</p>
  </body>
  <script src="js/dom-to-image.js" type="text/javascript"></script>
  <!-- <script src="js/megapix-image.js" type="text/javascript"></script> -->
  <script type="text/javascript" src="js/main.js"></script>
</html>
