// save image
function save_canvas(data, tagged=false) {
  // var data = c.toDataURL("image/png");
  if (tagged) {
    data = "tagged!!"+data
  }
  fetch("save-image.php", {
      method: "POST",
      mode: "no-cors",
      headers: {"Content-Type": "application/x-www-form-urlencoded"},
      body: data
  })  .then(response => response.text())
      .then(success => console.log(success))
      .catch(error => console.log(error));
}

// Change font
var textBox = document.querySelectorAll('.text-layer');

function changeFont(val){
  console.log(val);
  for(var i=0; i< textBox.length; i++){
    textBox[i].style.fontFamily =  val;
  }
}

// Change Color
var photo = document.getElementById('photo');
var output = document.getElementById('output');
var colorList =["blueviolet","cyan"]
var randCol = colorList[Math.round(Math.random() * 1)]
let mainColTag = document.getElementById("maincol")
changeColor(randCol)
function changeColor(col){
  mainColTag.innerHTML = ""
  let style = `:root{--col:${col};`
  mainColTag.innerHTML = style
  document.getElementById('colors').value = col
}

// Download button
function download(){
  if (!iOS()) {
    var image = new Image();
    var drawingCanvas = document.getElementById('sketchpad');
    image.src = drawingCanvas.toDataURL("image/jpeg", 1);

    // domtoimage.toJpeg(document.getElementById('output'), { quality: 0.9, bgcolor:"white" })
    // .then(function (dataUrl) {
    //     var link = document.createElement('a');
    //     link.download = 'a-party-to-print_invitation.jpeg';
    //     link.href = dataUrl;
    //     link.click();
    //   save_canvas(dataUrl)
    // }); 
  }else{
    var image = new Image();
    var drawingCanvas = document.getElementById('sketchpad');
    image.src = drawingCanvas.toDataURL("image/jpeg", 1);
    // var img = new Image();
    // img.src = dataUrl;
    // document.body.appendChild(image);
    save_canvas(drawingCanvas.toDataURL("image/jpeg", 1), true)
    document.getElementById('downloadLnk').innerHTML = "thank you"
    document.getElementById('downloadLnk').setAttribute('disabled','true')
    // return image;

  }
}


// CAMERA _ TAKE PICTURES
(() => {
  const width = 600; // We will scale the photo width to this
  let height = 0; // This will be computed based on the input stream
  let streaming = false;
  let video = null;
  let canvas = null;
  let photo = null;
  let startbutton = null;
  function showViewLiveResultButton() {
    if (window.self !== window.top) {
      // Ensure that if our document is in a frame, we get the user
      // to first open it in its own tab or window. Otherwise, it
      // won't be able to request permission for camera access.
      document.querySelector(".contentarea").remove();
      const button = document.createElement("button");
      button.textContent = "View live result of the example code above";
      document.body.append(button);
      button.addEventListener("click", () => window.open(location.href));
      return true;
    }
    return false;
  }

  function startup() {
    if (showViewLiveResultButton()) {
      return;
    }
    video = document.getElementById("video");
    canvas = document.getElementById("canvas");
    photo = document.getElementById("photo");
    camera = document.querySelector("#video");
    startbutton = document.getElementById("startbutton");

    if (!iOS()) {   
      navigator.mediaDevices
        .getUserMedia({ video: true, audio: false })
        .then((stream) => {
          video.srcObject = stream;
          video.play();
        })
        .catch((err) => {
          console.error(`An error occurred: ${err}`);
        });
  
      video.addEventListener(
        "canplay",
        (ev) => {
          if (!streaming) {
            height = video.videoHeight / (video.videoWidth / width);
            if (isNaN(height)) {
              height = width / (4 / 3);
            }
  
            video.setAttribute("width", width);
            video.setAttribute("height", height);
            canvas.setAttribute("width", width);
            canvas.setAttribute("height", height);
            streaming = true;
          }
        },
        false
      );
    }else{
      let downloadBtn = document.getElementById('downloadLnk')
      downloadBtn.removeAttribute('disabled')
    }
    startbutton.addEventListener(
      "click",
      (ev) => {
        if (!iOS()) {          
          takepicture();
          camera.style.display = "none";
          ev.preventDefault();
        }
      },
      false
    );

    clearphoto();
  }
  function clearphoto() {
    const context = canvas.getContext("2d");
    context.fillStyle = "#AAA";
    context.fillRect(0, 0, canvas.width, canvas.height);

    const data = canvas.toDataURL("image/png");
    photo.setAttribute("src", data);
  }

  function takepicture() {
    const context = canvas.getContext("2d");
    if (width && height) {
      canvas.width = width;
      canvas.height = height;
      // canvas.style.display ="block"
      context.drawImage(video, 0, 0, width, height);

      const data = canvas.toDataURL("image/png");
      photo.setAttribute("src", data);
      let downloadBtn =
      document.getElementById('downloadLnk')
      downloadBtn.removeAttribute('disabled')

    } else {
      clearphoto();
    }
  }
  window.addEventListener("load", startup, false);
})();

function iOS() {
  return [
    'iPad Simulator',
    'iPhone Simulator',
    'iPod Simulator',
    'iPad',
    'iPhone',
    'iPod'
  ].includes(navigator.platform)
  // iPad on iOS 13 detection
  || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
}

if (iOS()) {
  document.body.classList.add('sketchmode')
}

window.addEventListener('load', function () {
  var drawingCanvas = document.getElementById('sketchpad');
  var context = drawingCanvas.getContext('2d');
  
  var isIdle = true;
  context.fillStyle = randCol;
  context.fillRect(0, 0, drawingCanvas.width, drawingCanvas.height);
  function drawstart(event) {
    context.strokeStyle = "black";
    context.lineWidth = 10;
    context.lineCap = "round";
    context.beginPath();
    context.moveTo(event.pageX - drawingCanvas.offsetLeft, event.pageY - drawingCanvas.offsetTop);
    isIdle = false;
  }
  function drawmove(event) {
    if (isIdle) return;
    context.lineTo(event.pageX - drawingCanvas.offsetLeft, event.pageY - drawingCanvas.offsetTop);
    context.stroke();
  }
  function drawend(event) {
    if (isIdle) return;
    drawmove(event);
    isIdle = true;
  }
  function touchstart(event) { drawstart(event.touches[0]); }
  function touchmove(event) { drawmove(event.touches[0]); event.preventDefault(); }
  function touchend(event) { drawend(event.changedTouches[0]) }

  drawingCanvas.addEventListener('touchstart', touchstart, false);
  drawingCanvas.addEventListener('touchmove', touchmove, false);
  drawingCanvas.addEventListener('touchend', touchend, false);        

  drawingCanvas.addEventListener('mousedown', drawstart, false);
  drawingCanvas.addEventListener('mousemove', drawmove, false);
  drawingCanvas.addEventListener('mouseup', drawend, false);

}, false); // end window.onLoad
