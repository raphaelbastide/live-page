navigator.mediaDevices
.getUserMedia(constraints)
.then(function (stream) {
    cameraView.srcObject = stream
    cameraView.onloadedmetadata = function(e) {
        cameraView.play()
    }
  })
.catch(function (error) {
    alert(error)
})
