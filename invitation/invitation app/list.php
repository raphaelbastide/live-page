<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <meta name="robots" content="noindex, nofollow">
    <title>Images</title>
    <style media="screen">
    body{display: flex; flex-wrap: wrap; margin: 0; background:#ccc; justify-content:center;}
    figure{margin:1%; text-align:center;}
      figure img{width:400px; height:400px; object-fit: contain;}
      img[src~="drawing"] {
        border-radius: 100%;
        filter: blur(5px);
        width: 240px;
        height: 260px;
      }
    </style>
  </head>
  <body>
<?php
$directory = "invitation-images";
$imgs = rglob($directory."/{*.jpeg}", GLOB_BRACE);
foreach (array_reverse($imgs) as $image) {
  echo "<figure>";
  echo "<img src='$image'/>";
  echo "<p>".basename($image, ".svg").PHP_EOL."</p>";
  echo "</figure>";
}
function rglob($pattern, $flags = 0) {
  $files = glob($pattern, $flags);
  foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
    $files = array_merge($files, rglob($dir.'/'.basename($pattern), $flags));
  }
  return $files;
}

?>
</body>
</html>
