<?php
  $infos = json_decode(file_get_contents('php://input'), true);
  $img = str_replace('data:image/jpeg;base64,', '', $infos['data']);
  $img = str_replace(' ', '+', $img);
  $data = base64_decode($img);
  
  if (!file_exists("/")) {
    mkdir("images/", 0777, true);
  }
  $file = "images/image-".$infos['id'].'.jpg';  
  $success = file_put_contents($file, $data);
  print $success ? $file.' saved.' : 'Unable to save the file.';