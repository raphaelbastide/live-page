<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>A party to Print | Webcam 2 book</title>
    <link rel="stylesheet" href="font/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="css/main.css" />
  </head>
  <body>
    <div class="contentarea">
      <div id="output">
        <canvas id="canvas"></canvas>
        <img id="photo">
        <video id="video">Video stream not available.</video>
      </div>
    </div>
  </body>
  <script src="dom-to-image.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/main.js"></script>
</html>

