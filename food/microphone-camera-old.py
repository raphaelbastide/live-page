#!/usr/bin/env python3

# prerequisites: as described in https://alphacephei.com/vosk/install and also python module `sounddevice` (simply run command `pip install sounddevice`)
# Example usage using Dutch (nl) recognition model: `python test_microphone.py -m nl`
# For more help run: `python test_microphone.py -h`

#install opencv `pip install opencv-python`

import argparse
import queue
import sys
import sounddevice as sd
import json
# uncomment this part to take picture
import cv2
import os
import re
from datetime import datetime

from vosk import Model, KaldiRecognizer

q = queue.Queue()
jelloWords = ['jello', 'hello', 'shadow', 'hero', 'heroes', 'yellow', 'jail', 'gallo', 'ditto', 'mellow']

def save_frame_camera_key(device_num, dir_path, name, ext='jpg', delay=1, window_name='frame'):
    cam = cv2.VideoCapture(device_num)

    result, image = cam.read()
    if result:
  
        # showing result, it take frame name and image 
        # output
        # cv2.imshow(window_name, image)

        # saving image in local storage
        cv2.imwrite('{}-{}.{}'.format("image", name, ext), image)
#        n+=1

        # If keyboard interrupt occurs, destroy image 
        # window
        #cv2.waitKey(0)
        #cv2.destroyWindow(window_name)

def int_or_str(text):
    """Helper function for argument parsing."""
    try:
        return int(text)
    except ValueError:
        return text

def callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    if status:
        print(status, file=sys.stderr)
    q.put(bytes(indata))

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument(
    "-l", "--list-devices", action="store_true",
    help="show list of audio devices and exit")
args, remaining = parser.parse_known_args()
if args.list_devices:
    print(sd.query_devices())
    parser.exit(0)
parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    parents=[parser])
parser.add_argument(
    "-f", "--filename", type=str, metavar="FILENAME",
    help="audio file to store recording to")
parser.add_argument(
    "-d", "--device", type=int_or_str,
    help="input device (numeric ID or substring)")
parser.add_argument(
    "-r", "--samplerate", type=int, help="sampling rate")
parser.add_argument(
    "-m", "--model", type=str, help="language model; e.g. en-us, fr, nl; default is en-us")
args = parser.parse_args(remaining)

try:
    if args.samplerate is None:
        device_info = sd.query_devices(args.device, "input")
        # soundfile expects an int, sounddevice provides a float:
        args.samplerate = int(device_info["default_samplerate"])
        
    if args.model is None:
        model = Model(lang="en-us")
    else:
        model = Model(lang=args.model)

    if args.filename:
        dump_fn = open(args.filename, "wb")
    else:
        dump_fn = None

    with sd.RawInputStream(samplerate=args.samplerate, blocksize = 8000, device=args.device,
            dtype="int16", channels=1, callback=callback):
        print("#" * 80)
        print("Press Ctrl+C to stop the recording")
        print("#" * 80)

        rec = KaldiRecognizer(model, args.samplerate)
        while True:
            data = q.get()
            if rec.AcceptWaveform(data):
                # print(rec.Result())
                results = json.loads(rec.Result())
                print(results['text']) 
                with open("text.txt","a") as file:
                    file.write(results['text'])
                # find word in text and take picture from camera and add it in the text
                r = re.compile('|'.join([r'\b%s\b' % w for w in jelloWords]), flags=re.I)
                jelloFinds = r.findall(results['text'])
 
                for word in jelloFinds:
                    print('take picture')
                    dt = datetime.now()
                    ts = datetime.timestamp(dt)
                    # uncomment this part to take picture
                    save_frame_camera_key(0, '/', ts)
                    with open("text.txt","a") as file:
                        file.write('<figure class="jello"><img src="food/image-'+str(ts)+'.jpg"></figure>')
            # else:
                # print(rec.PartialResult())
            if dump_fn is not None:
                dump_fn.write(data)

except KeyboardInterrupt:
    print("\nDone")
    parser.exit(0)
except Exception as e:
    parser.exit(type(e).__name__ + ": " + str(e))





