<!DOCTYPE html PUBLIC>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <title>A Party to print</title>
    <link href="css/pagedjs/reset.css" rel="stylesheet" type="text/css">
    <link href="css/pagedjs/pagedjs.css" rel="stylesheet" type="text/css">
    <link href="css/shared.css" rel="stylesheet" type="text/css">
    <link id="style-screen" href="css/style-screen.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/style-print.css" media="print" rel="stylesheet" type="text/css">
    <link href="css/01-invitation.css" rel="stylesheet" type="text/css">
    <link href="css/02-food.css" rel="stylesheet" type="text/css">
    <link href="css/03-music.css" rel="stylesheet" type="text/css">
    <link href="css/04-moritz.css" rel="stylesheet" type="text/css">
</head>
<body class="flatplan">
    <?php 
      @include "utils.php";
    ?>

    <section id="cover" class="page">
      <!-- <h1>Blur Factory</h1> -->
      <figure>
        <img src="images/cover.png">
      </figure>
    </section>
    <section id="organisation-group" data-chapter="3">
        <div class="content">
          <div class="grid-images images-invitation">
            <?php 
                $dirlistInvit = getFileList("invitation");
                array_multisort(array_column($dirlistInvit, "name"), SORT_ASC, $dirlistInvit );
                $count = count($dirlistInvit);
            ?>
            <?php foreach ($dirlistInvit as $file):?>
              <?php 
                if($file['type'] == 'image/jpeg'):
                  $filenameClean = str_replace('invitation/', '', $file['name'],);
                  $filenameClean = str_replace(' drawing ', '', $filenameClean,);
                  ?>
                  <figure>
                      <img src="<?php echo $file['name']?>">
                      <figcaption><?= $filenameClean ?></figcaption>
                  </figure>
                <?php endif;?>
            <?php endforeach;?>  
            </div> 
        </div>
    </section>
    <section id="entrance">
        <h1 style="margin-bottom:0;">Blur Factory</h1>
        <h1>A Party to Print</h1>
    </section>
    <section id="table-of-contents" class="page-right">
        <h2>**** Index ****</h2>
    </section>
    <section class="page-left">
      <pre>

                     P A R T Y
  
                    /    |    \
                   /     |     \
          ---------      |      ---------
        /                |                \
    TEAM&nbsp;1             TEAM&nbsp;2           TEAM&nbsp;3
   (music)             (food)           (organisation)
      |                  |                  |
      |                  |                  |
      |                  |                  |
   DEVICE 1           DEVICE&nbsp;2           DEVICE&nbsp;3
      |                  |                  |
      |                  |                  |
      |                  |                  |
.------.------.    .------.------.    .------.------.
|      |      |    |      |      |    |      |      |
|  m   |   m  |    |   f  |   f  |    |   p  |   p  |
|      |      |    |      |      |    |      |      |
‘------‘------‘    ‘------‘------‘    ‘------‘------‘
music section      food section       orga section
        \                |               /
         \               |              /
           ---------     |    ---------
                     \   |   /

                     ,  :   ‘.
                    . B O O K  .
                      . ‘  :  ‘
</pre>
    </section>
    <section id="introduction" class="chapter page-right" data-chapter="1">
        <h1>Introduction</h1>
        <h1>A party to print</h1>
        <p class="columns-3">
          The book consists of chapters composing the different point of views of a party held at the HfG Karlsruhe on 29/06/23: music, lighting, scenography, food and drinks, and organisation. Students of the seminar‚ *A Party to Print* have developed and designed dedicated digital protocols and tools to live document the event, using automation, HTML to print, single source publishing, live collaboration, generative layout and content. Those experimental devices were made possible thanks to network of free and open source software.  Because of the structure of the HfG building and the ambiguous form of the seminar outcome -between print and digital- we decided together to name the party: *Blur Factory*.
        </p>
        <br>
        <br>
        <p>
          *The party is the book and the book is the party*
        </p>
    </section>
    <section class="page-left">
      <figure class="image-title image-left">
        <img src="images/floor_plan.png">
      </figure>
      <figure class="image-title image-right">
        <img src="images/floor_plan.png">
      </figure>
    </section>
    <section id="food-group" class="chapter page-left" data-chapter="4">
       <div class="chapter-title">
          <h1>The shadows of heroes are jello</h1>
          <h2>The shadows</h2>
          <div class="map-title">
            <h3>* Room 1 *</h3>
            <figure>
              <img src="images/food_map.png">
            </figure>
          </div>
          <figure class="image-title image-left">
            <img src="images/food_chapter.png">
          </figure>
          <figure class="image-title image-right">
            <img src="images/food_chapter.png">
          </figure>
       </div>
        <div class="content">
          <div class="chapter-intro break">
            <p>
              Hey, what's his place?<br>
              It's a buffet?<br>
              Is it though?<br>
              Well, you have eyes. You see. Even though it's a bit blurry. <br>
              Mist? Smoke? <br>
              Mood.<br>
              It's like a joke. Like seriously, jello?<br>
              Jello 🙂<br>
              You disgusting people.
              </p>

              <h3>* Ingredients *</h3>
              <p>
              You & Us happily together<br>
              Party spirit<br>
              Your spoken word <br>
              Your will to consume a vile amount of jello
              </p>

              <h3>* Instructions *</h3>
              <p>
              Say hello,<br>
              Say yellow,<br>
              Say  things that sound like fellow,<br>
              Say anything that isn't mellow,<br>
              Repeat, repeat the word JELLO! 
              </p>

              <h3>* Actual Ingredients *</h3>
              <p>
              agaragar<br>
              vodka<br>
              strawberries<br>
              raspberries<br>
              currants<br>
              mango<br>
              oranges<br>
              lemons<br>
              sugar<br>
              waldmeister sirup<br>
              strawberry sirup<br>
              blue, red and green food coloring
              </p>
          </div>
          <div class="jello-wrapper" id="livedata">
              <?php $text = file_get_contents('food/text.txt');?>
              <p><?php echo $text;?></p>
          </div> 
        </div>
    </section>
    <section id="music-group" class="chapter" data-chapter="5">
       <div class="chapter-title">
          <h1>Traces & Tracks</h1>
          <h2>T & T</h2>
          <div class="map-title">
            <h3>* Room 2 *</h3>
            <figure>
              <img src="images/music_map.png">
            </figure>
          </div>
          <figure class="image-title image-left">
            <img src="images/music_chapter.png">
          </figure>
          <figure class="image-title image-right">
            <img src="images/music_chapter.png">
          </figure>
       </div>
        <div class="content">
          <div class="chapter-intro" style="width:calc(66% - 2.5mm); position: absolute; top: 5cm; left: calc(33% + 2.5mm); text-align: justify;">
            <p>These are all the moments of the party dancing, grooving and vibrating with us, swarm of blurs.</p>
            <p>You are some of them, or not, it is surely part of the clump, we are all together, we are all linked by the sound waves.</p>
          </div>
            <?php 
            $dirlistKimin = getFileList("music");
            array_multisort(array_column($dirlistKimin, "name"), SORT_ASC, $dirlistKimin );
            $count = count($dirlistKimin);
            $nbOfLines = 26;
            $counter = 0;
            $nbOfKiminPages = intval($count / $nbOfLines) + 1;
            $dirlistKimintest =  array_chunk($dirlistKimin,(int)ceil(count($dirlistKimin)/3));
            //print_r($dirlistKimintest[0]);
            

            // get the first half part of the files
            // $dirlistKimin50 = array_slice($dirlistKimin, 0, $count/2);
            // get the last half part of the files
            // $dirlistKimin100 = array_slice($dirlistKimin, $count/2, $count);
            for ($i=0; $i < $nbOfKiminPages; $i++):
              
            ?>

              <div class="kimin kimin-left">
                <?php 
                foreach ($dirlistKimintest[$i] as $file):?>
                  <figure>
                    <img src="<?php echo $file['name']?>">
                  </figure>
                <?php endforeach;?>
              </div>
              <div class="kimin kimin-right">
                <?php foreach ($dirlistKimintest[$i] as $file):?>
                  <figure>
                    <img src="<?php echo $file['name']?>">
                  </figure>
                <?php endforeach;?>
              </div>
              <?php endfor; ?>
            <!-- loop throught the last 24 files to create a spread -->
        </div>
        <!-- <div class="tracklist">
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
          <p>Mike Oldfield * Moonlight Shadow</p>
          <p>Take me home, country roads * Yuji Nomi</p>
        </div> -->
    </section>
<!--     <section id="sceno" class="chapter" data-chapter="6">
       <div class="chapter-title">
          <h1>Sceno</h1>
          <h2>Sceno</h2>
          <div class="map-title">
          <h3>* Room 4 *</h3>
          <figure>
            <img src="images/map-jello.png">
          </figure>
          </div>
          <figure class="image-title image-left">
            <img src="images/title.png">
          </figure>
          <figure class="image-title image-right">
            <img src="images/title.png">
          </figure>
          <div class="content page-left">
            <figure class="image-big image-left-pos image-bottom-pos">
              <img src="food/image-1687955439.297334.jpg">
            </figure>
            <figure class="image-medium image-right-pos image-top-pos">
              <img src="food/image-1687955439.297334.jpg">
            </figure>
            <figure class="image-small image-left-pos image-top-pos">
              <img src="food/image-1687955439.297334.jpg">
            </figure>
             <figure class="image-medium image-left-pos image-bottom-pos">
              <img src="food/image-1687955439.297334.jpg">
            </figure>
          </div>
       </div>
    </section> -->
    <section id="credits" class="chapter page-left" data-chapter="6">
        <h1>Credits</h1>
<pre>
Seminar:          *A Party to Print* at HfG
                  Karlsruhe in the Summersemester
                  2023

Professors:       Sarah Garcin
                  Raphaël Bastide

Students:         Kevin Beckmann
                  Rahel Diederich
                  Hoin Ji
                  Kimin Han
                  Maximilian Zschiesche
                  Melanie Wisser
                  Róza Velkei
                  Lea Moescheid
                  Moritz Schneider
                  Francesco Perale
                  Anna Katalin Szilagyi

Used Font:        Ocr-Pbi by Luuse, typotheque.luuse.fun

Number of 
copies:           00
        
Design process:   Gathering datas and pictures from
                  the party and transform into
                  communication.

Graphic Design:   Moritz Schneider, Roza Velkei,
                  Francesco Perale

Party:            29/06/2023 at HfG Karlsruhe

</pre>
    </section>
    <div class="page"></div>
    <section id="backcover">
      <!-- <h1>Backcover</h1> -->
    </section>
</body>
<script type="text/javascript" src="js/paged.polyfill.js"></script>
<script type="text/javascript" src="js/main.js"></script>
</html>