let autoscrollActive = false;


let guidelines = false;
// Show / Hide Guidelines by ctrl + d
document.body.addEventListener('keydown', function(e){
  // e.preventDefault()
  let target = e.target;
  let margins = document.querySelectorAll('.pagedjs_page_content');
  let cols = document.querySelectorAll('.gui-column');
  if (e.ctrlKey && e.key == 'b') { 
    if(guidelines){
      console.log("Hide guidelines");
      guidelines = false;
      margins.forEach(function(el, i) {
        el.style.outline = "none";
      });
      cols.forEach(function(col, i) {
        col.style.outline = "none";
      }); 
    }
    else{
      console.log("Display guidelines");
      guidelines = true;
      margins.forEach(function(el, i) {
        el.style.outline = "1px solid magenta";
      });
      cols.forEach(function(col, i) {
        col.style.outline = "1px solid cyan";
      });
    }

    e.preventDefault();
  }
});


class toc extends Paged.Handler {
    constructor(chunker, polisher, caller) {
      super(chunker, polisher, caller);
    }

    beforeParsed(content){         
      createToc({
        content: content,
        tocElement: '#table-of-contents',
        titleElements: [ '.chapter h1' ]
      });
    }
    
  }
Paged.registerHandlers(toc);


// TOC
function createToc(config){
    const content = config.content;
    const tocElement = config.tocElement;
    const titleElements = config.titleElements;
    
    let tocElementDiv = content.querySelector(tocElement);
    let tocUl = document.createElement("ul");
    tocUl.id = "list-toc-generated";
    tocElementDiv.appendChild(tocUl); 
    // console.log(tocElementDiv);

    // add class to all title elements
    let tocElementNbr = 0;
    for(var i= 0; i < titleElements.length; i++){
        	
        let titleHierarchy = i + 1;
        let titleElement = content.querySelectorAll(titleElements[i]);


        titleElement.forEach(function(element) {
            // add classes to the element
            element.classList.add("title-element");
            element.setAttribute("data-title-level", titleHierarchy);

            // add id if doesn't exist
            tocElementNbr++;
            let idElement = element.id;
            if(idElement == ''){
                element.id = 'title-element-' + tocElementNbr;
            } 
            let newIdElement = element.id;

        });

    }

    // create toc list
    let tocElements = content.querySelectorAll(".title-element");  

    for(var i= 0; i < tocElements.length; i++){
        let tocElement = tocElements[i];

        let tocNewLi = document.createElement("li");

        // Add class for the hierarcy of toc
        tocNewLi.classList.add("toc-element");
        tocNewLi.classList.add("toc-element-level-" + tocElement.dataset.titleLevel);

        // Keep class of title elements
        let classTocElement = tocElement.classList;
        for(var n= 0; n < classTocElement.length; n++){
            if(classTocElement[n] != "title-element"){
                tocNewLi.classList.add(classTocElement[n]);
            }   
        }
        // Create the element
        tocNewLi.innerHTML = '<a href="#' + tocElement.id + '">' + tocElement.innerHTML + '</a>';
        tocUl.appendChild(tocNewLi);  
    }

}

// add columns grid layout (for developpement)
class guiCol extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  afterRendered(pages) {
    for(let i=0; i<pages.length; i++){
      let colWrapper = document.createElement("div");
      colWrapper.classList.add('col-wrapper');
      for(let a=0; a<3; a++){
        let col = document.createElement("div");
        col.classList.add('gui-column');
        colWrapper.appendChild(col);
      }
      pages[i].area.appendChild(colWrapper);
    }
  }
}

Paged.registerHandlers(guiCol);


// add corner image on each page starting with page …
class CornerImage extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  afterRendered(pages) {
    const startPage = 14;
    let counter = 84;
    for(let i=startPage; i<pages.length; i++){
    	let figure = document.createElement("figure");
    	let glass = document.createElement("div");
    	figure.classList.add('corner-image');
    	glass.classList.add('glass');
    	let imagePath = "sceno/images/image-" + counter + ".jpg";
      figure.innerHTML = `<img src="`+imagePath+`" onerror="this.style.display = 'none'">`;
      let ifRight = pages[i].element.classList.contains('pagedjs_right_page');
      if(ifRight){
        pages[i].area.appendChild(figure);
        pages[i].area.appendChild(glass);
        counter ++;
      }
     }
  }
}
Paged.registerHandlers(CornerImage);


// Tracklist

class tracklist extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  afterRendered() {
    let trackItems = document.querySelectorAll('.tracklist p')
    let i = 0
    trackItems.forEach(item => {      
      item.style = `--nbr:${i};`
      i++
    });
  }
}
Paged.registerHandlers(tracklist);


class spots extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  afterRendered() {
    let t = 700
    function spotLoop() {
      createSpot()
      t = Math.floor(Math.random() * (5000 - 700 + 1) + 700)    
      setTimeout(function() {
        spotLoop();
      }, t);
    }
  spotLoop()
  }
}
// Paged.registerHandlers(spots);


// spots effects

var sizes = ['small','medium','large']
var cols = ['0,255,255']

function createSpot(){
  t = Math.round(Math.random() * 100)
  l = Math.round(Math.random() * 100)
  c = Math.floor(Math.random() * cols.length)
  s = Math.floor(Math.random() * sizes.length)
  col = cols[c]
  size = sizes[s]
  var spot = document.createElement('div')
  spot.classList.add('spot')
  spot.classList.add(size)
  spot.style.top = t+'%'
  spot.style.left = l+'%'
  spot.style.position = 'fixed'
  spot.style.backgroundImage = 'radial-gradient(rgba('+col+',1) 0%, transparent 50%)'

  document.body.appendChild(spot)
  setTimeout(function () {
    spot.remove()
  }, 600);
}


class autoscroll extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  afterRendered() {
    // Autoscrooll and reload

    let scrollSpeed = 1;
    let scrollInterval = 5;
    let bottom = document.querySelector('#backcover')
    function autoScrollAndReload() {
      window.scrollBy(0, scrollSpeed)
      console.log(isScrolledIntoView(bottom));
      if (isScrolledIntoView(bottom)) {
        setTimeout(() => {
          location.reload();          
        }, 15000);
        return
      }
    }
    if (autoscrollActive) {
      setTimeout(function(){
        let scrollingInterval = setInterval(autoScrollAndReload, scrollInterval);
      }, 1000);  
    }

  }
}
Paged.registerHandlers(autoscroll);


function isScrolledIntoView(el) {
  var rect = el.getBoundingClientRect();
  var elemTop = rect.top;
  var elemBottom = rect.bottom;
  var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
  return isVisible;
}


// timestamp for food images
class foodTimestamp extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {
    let figs = content.querySelectorAll('.jello-wrapper figure')
    figs.forEach(fig => {
      let caption = document.createElement('figcaption')
      let timestamp = fig.querySelector('img').getAttribute('src')
      timestamp = timestamp.replace('food/image-','').replace('.jpg','').split('.')[0]
      let h = new Date(Number(timestamp) * 1000).getHours();
      let m = new Date(Number(timestamp) * 1000).getMinutes();
      h = (h<10) ? '0' + h : h;
      m = (m<10) ? '0' + m : m;
      let output = h + ':' + m;
      caption.innerHTML = output
      fig.appendChild(caption)
    });
  }
}
Paged.registerHandlers(foodTimestamp);
